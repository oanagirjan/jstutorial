$(document).ready(function(){
    $('body').load(fetchIssues());
    $('.addButton').click(e=>saveIssue(e));
    function fetchIssues() {
        let issues = JSON.parse(localStorage.getItem('issues'));
        let $issuesList = $('#issuesList');
        if(issues)
            issues.map(
                (issue) =>{
                    let id = issue.id;
                    let desc = issue.description;
                    let severity = issue.severity;
                    let assignedTo = issue.assignedTo;
                    let status = issue.status;

                    $issuesList.append(`<div class="well">
                        <h6>Issue ID: ${id} </h6>
                        <p><span class="label label-info">${status}</span></p>
                        <h3>${desc}</h3>
                        <p><span class="glyphicon glyphicon-time"></span>${severity}
                        <span class="glyphicon glyphicon-user"></span>${assignedTo}</p>
                        <a href="#" class="btn btn-warning" >Close</a>
                        <a href="#" class="btn btn-danger" >Delete</a>
                        </div>`);
                }
        )
    }

    function saveIssue(e) {
        let issueId = chance.guid();
        let issueDesc = $('#issueDescInput').val();
        let issueSeverity = $('#issueSeverityInput').val();
        let issueAssignedTo = $('#issueAssignedToInput').val();
        let issueStatus = 'Open';
        let issue = {
            id: issueId,
            description: issueDesc,
            severity: issueSeverity,
            assignedTo: issueAssignedTo,
            status: issueStatus
        };

        let issues = localStorage.getItem('issues')? JSON.parse(localStorage.getItem('issues')) : [];
        issues.push(issue);
        localStorage.setItem('issues', JSON.stringify(issues));

        $('#issueInputForm').reset();

        fetchIssues();

        e.preventDefault();
    }

    function setStatusClosed (id) {
        let issues = JSON.parse(localStorage.getItem('issues'));

        issues.map(
            issue =>{
                if(issue.id===id){
                    issue.status='Closed';
                }
            }
        );

        localStorage.setItem('issues', JSON.stringify(issues));

        fetchIssues();
    }

    function deleteIssue (id) {
        let issues = JSON.parse(localStorage.getItem('issues'));

        issues.map(
            (issue, index) =>{
                if(issue.id===id){
                    issues.splice(index, 1);
                }
            }
        );

        localStorage.setItem('issues', JSON.stringify(issues));

        fetchIssues();
    }
});