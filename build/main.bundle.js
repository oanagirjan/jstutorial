'use strict';

$(document).ready(function () {
    $('body').load(fetchIssues());
    $('.addButton').click(function (e) {
        return saveIssue(e);
    });
    function fetchIssues() {
        var issues = JSON.parse(localStorage.getItem('issues'));
        var $issuesList = $('#issuesList');
        if (issues) issues.map(function (issue) {
            var id = issue.id;
            var desc = issue.description;
            var severity = issue.severity;
            var assignedTo = issue.assignedTo;
            var status = issue.status;

            $issuesList.append('<div class="well">\n                        <h6>Issue ID: ' + id + ' </h6>\n                        <p><span class="label label-info">' + status + '</span></p>\n                        <h3>' + desc + '</h3>\n                        <p><span class="glyphicon glyphicon-time"></span>' + severity + '\n                        <span class="glyphicon glyphicon-user"></span>' + assignedTo + '</p>\n                        <a href="#" class="btn btn-warning" >Close</a>\n                        <a href="#" class="btn btn-danger" >Delete</a>\n                        </div>');
        });
    }

    function saveIssue(e) {
        var issueId = chance.guid();
        var issueDesc = $('#issueDescInput').val();
        var issueSeverity = $('#issueSeverityInput').val();
        var issueAssignedTo = $('#issueAssignedToInput').val();
        var issueStatus = 'Open';
        var issue = {
            id: issueId,
            description: issueDesc,
            severity: issueSeverity,
            assignedTo: issueAssignedTo,
            status: issueStatus
        };

        var issues = localStorage.getItem('issues') ? JSON.parse(localStorage.getItem('issues')) : [];
        issues.push(issue);
        localStorage.setItem('issues', JSON.stringify(issues));

        $('#issueInputForm').reset();

        fetchIssues();

        e.preventDefault();
    }

    function setStatusClosed(id) {
        var issues = JSON.parse(localStorage.getItem('issues'));

        issues.map(function (issue) {
            if (issue.id === id) {
                issue.status = 'Closed';
            }
        });

        localStorage.setItem('issues', JSON.stringify(issues));

        fetchIssues();
    }

    function deleteIssue(id) {
        var issues = JSON.parse(localStorage.getItem('issues'));

        issues.map(function (issue, index) {
            if (issue.id === id) {
                issues.splice(index, 1);
            }
        });

        localStorage.setItem('issues', JSON.stringify(issues));

        fetchIssues();
    }
});
